import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/Login',
    name: 'Login',
    component: () => import('../views/Login')
  },
  {
    path: '/Contact',
    name: 'Contact',
    component: () => import('../views/Contact')
  },
  {
    path: '/Form',
    name: 'Form',
    component: () => import('../views/Form')
  },
  {
    path: '/Announce',
    name: 'Announce',
    component: () => import('../views/Announce')
  },
  {
    path: '/About',
    name: 'About',
    component: () => import('../views/About')
  },
  {
    path: '/officer',
    name: 'officer',
    component: () => import('../views/officer')
  },
  {
    path: '/professor',
    name: 'professor',
    component: () => import('../views/professor')
  },
  {
    path: '/student',
    name: 'student',
    component: () => import('../views/student')
  },
  {
    path: '/establishments',
    name: 'establishments',
    component: () => import('../views/Establishments')
  },
  {
    path: '/hours',
    name: 'hours',
    component: () => import('../views/Hours')
  },
  {
    path: '/edits',
    name: 'edits',
    component: () => import('../views/Students/edits')
  },
  {
    path: '/students',
    name: 'students',
    component: () => import('../views/Students')
  },
  {
    path: '/editv',
    name: 'editv',
    component: () => import('../views/Trains/editv')
  },
  {
    path: '/trains',
    name: 'trains',
    component: () => import('../views/Trains')
  },
  {
    path: '/announces',
    name: 'announces',
    component: () => import('../views/Announces')
  },
  {
    path: '/professors',
    name: 'professors',
    component: () => import('../views/Professor/addProfessor')
  },
  {
    path: '/statuss',
    name: 'statuss',
    component: () => import('../views/Status/checkStatus')
  },
  {
    path: '/checkgrade',
    name: 'checkgrade',
    component: () => import(/* webpackChunkName: "about" */ '../views/Grades/checkGrade.vue')
  },
  {
    path: '/checkhour',
    name: 'checkhour',
    component: () => import(/* webpackChunkName: "about" */ '../views/Hours/checkHour')
  },
  {
    path: '/coperations',
    name: 'coperations',
    component: () => import('../views/Coperations')
  },
  {
    path: '/register',
    name: 'register',
    component: () => import('../views/Coperations/register.vue')
  },
  {
    path: '/index',
    name: 'index',
    component: () => import('../views/index.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
